<!DOCTYPE html>
<html lang="en">
<head>
        
        
    <style>html {
        height: 100%;
      }
      .header{
    position:absolute;
    width: 450px;
    height: 500px;
    border-radius: 10px;
    left:35%;
    padding: 20px;
    box-sizing: border-box;
    background: #ececec;
    font-family: 'Courier New', Courier, monospace;
    font-size: 17px;
    letter-spacing: 2px;
    color: #0d1a4a;
    border-radius: 40px;
    border-right-style: inset;
    border-right-color: #0d1a4a;
    border-bottom-style: inset;
    border-bottom-color:#0d1a4a;
    box-shadow: 0 20px 25px rgba(0,0,0,.6);
  }
      body {
        margin:0;
        padding:0;
        font-family: sans-serif;
        background:#ecf0f3;
        color: #ececec;
      }
      
      .container {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 400px;
        font-family: 'Courier New', Courier, monospace;
        padding: 40px;
        transform: translate(-50%, -50%);
        /* background:linear-gradient(brown,#141e30); */
        box-sizing: border-box;
        box-shadow: 0 20px 25px rgba(0,0,0,.6);
        border-radius: 20px;
        color:#0d1a4a ;
        border-top-style: outset;
        border-top-color:white;
        border-left-style:outset;
        border-left-color: #0d1a4a;
        border-bottom-style: inset;
        border-bottom-color: #0d1a4a;
      }
      .container .box {
        position: relative;
      }
      
      .container .box input {
        width: 100%;
        padding: 10px 0;
        font-size: 16px;
        color: #0d1a4a;
        margin-bottom: 30px;
        border: none;
        border-bottom: 1px solid #fff;
        outline: none;
        background: transparent;
      }
      .container .box label {
        position: absolute;
        top:0;
        left: 0;
        padding: 10px 0;
        font-size: 16px;
        color: #fff;
        pointer-events: none;
        transition: .5s;
      }
      
      .container .box input:focus ~ label,
      .container .box input:valid ~ label {
        top: -20px;
        left: 0;
        color: #141e30;
        font-size: 12px;
      }
      
      .container form a {
        position: relative;
        display: inline-block;
        padding: 10px 90px;
        ;
        font-size: 16px;
        text-decoration: none;
        text-transform: uppercase;
        overflow: hidden;
        transition: .5s;
        margin-top: 40px;
        letter-spacing: 10px;
        background: #141e30;
        color: #fff;
      }
      
      .container a:hover {
        background:#ecf0f3;
        color: #02022a;
        border-radius: 5px;
        box-shadow: 0 0 5px ,#0d1a4a,#3f547f
                    0 0 25px #141e30,
                    0 0 50px #0d1a4a,#3f547f,
                    0 0 100px #141e30;
      }
      
  .container a span {
    position: absolute;
    display: block;
  }
  
  .container a span:nth-child(1) {
    top: 0;
    left: -100%;
    width: 100%;
    height: 2px;
    background: linear-gradient(90deg, transparent, #141e30);
    animation: btn-anim1 30s linear infinite;
  }
  
  @keyframes btn-anim1 {
    0% {
      right: -100%;
    }
    50%,100% {
      left: 100%;
    }
  }
  
  .container a span:nth-child(2) {
    top: -100%;
    right: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(180deg, transparent, #141e30);
    animation: btn-anim2 30s linear infinite;
    animation-delay: .25s
  }
  
  @keyframes btn-anim2 {
    0% {
      top: -100%;
    }
    50%,100% {
      top: 100%;
    }
  }
  
  .container a span:nth-child(3) {
    bottom: 0;
    right: -100%;
    width: 100%;
    height: 2px;
    background: linear-gradient(270deg, transparent, #141e30);
    animation: btn-anim3 30s linear infinite;
    animation-delay: .5s
  }
  
  @keyframes btn-anim3 {
    0% {
      right: -100%;
    }
    50%,100% {
      right: 100%;
    }
  }
  
  .container a span:nth-child(4) {
    bottom: -100%;
    left: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(360deg, transparent, #141e30);
    animation: btn-anim4 30s linear infinite;
    animation-delay: .75s
  }
  
  @keyframes btn-anim4 {
    0% {
      bottom: -100%;
    }
    50%,100% {
      bottom: 100%;
    }
  }
  .submit{
  border-style: hidden;
  background-color: transparent;
  font-size: large;
  font-style:unset;
  color: white;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 10px;
  margin-bottom: 10px;
  letter-spacing:4px;
  
}
.submit:hover{
  color: #04083b;
}
      </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="header">
     <b> QUESTION 18:</b><br><br>
     WAP to accept two integers and check whether they are equal or not.
     
    </div>
    
        <div class="container">
            <form action="Question18.html" method="POST">
          <?php
              if($_SERVER['REQUEST_METHOD']=='POST'){

                $x = $_POST['x'];
                $y = $_POST['y'];
                print "INPUT:<BR><BR>";
                print "X"." = ".$x."<br>";  
                print "Y"." = ".$y."<br><br>";
                print "OUTPUT: <BR><br>";
                if($x==$y){
                    echo "The Two Numbers are Equal";
                }else{
                    echo "The Two Numbers are not Equal";
                }

              }

              ?>
                
              <a href="">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              <input type="submit" value="GO BACK" class="submit">
              </a>
            </form>
              </div>
                        
        
    
</body>
</html>